from sentence_transformers import SentenceTransformer, util

from resources.FAQ import FAQ, ANSWERS

class SentenceDeterminant:
    model = None
    embeddings = None
    answers = []

    def __init__(self):
        if not SentenceDeterminant.model:
            SentenceDeterminant.model = SentenceTransformer('all-MiniLM-L6-v2')
            questions = []
            for question in FAQ:
                questions.append(question)
                SentenceDeterminant.answers.append(FAQ[question])
            SentenceDeterminant.embeddings = SentenceDeterminant.model.encode(questions)

    def get_answer(self, sentence):
        emb = SentenceDeterminant.model.encode(sentence)
        cos_sim = util.cos_sim(SentenceDeterminant.embeddings, [emb])
        sentence_combinations = []
        for i in range(0, len(cos_sim)):
            sentence_combinations.append([i, cos_sim[i]])
        closest_answer = sorted(sentence_combinations, key=lambda x: x[1], reverse=True)[0]
        print(float(closest_answer[1]), closest_answer)
        if float(closest_answer[1]) > 0.2:
            return ANSWERS[SentenceDeterminant.answers[closest_answer[0]]]
        else:
            return "Я не могу ответить на данный вопрос, позвоните оператору"
