from services.sentence_determinant import SentenceDeterminant

user_answer = ""
sentence_determinant = SentenceDeterminant()
while user_answer != "q":
    user_answer = input("Задайте вопрос: ")
    print(sentence_determinant.get_answer(user_answer))
